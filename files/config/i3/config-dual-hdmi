# i3 config file (v4)

set $mod Mod4
font pango:Noto Sans 9 

# Use Mouse+$mod to drag floating windows to their wanted position
floating_modifier $mod

# start a terminal
bindsym $mod+Return exec i3-sensible-terminal

# kill focused window
bindsym $mod+Shift+q kill

# start dmenu (a program launcher)
bindsym $mod+d exec "rofi -show drun"

# change focus
bindsym $mod+h focus left
bindsym $mod+j focus down
bindsym $mod+k focus up
bindsym $mod+l focus right

# alternatively, you can use the cursor keys:
bindsym $mod+Left focus left
bindsym $mod+Down focus down
bindsym $mod+Up focus up
bindsym $mod+Right focus right

# move focused window
bindsym $mod+Shift+h move left
bindsym $mod+Shift+j move down
bindsym $mod+Shift+k move up
bindsym $mod+Shift+l move right

# alternatively, you can use the cursor keys:
bindsym $mod+Shift+Left move left
bindsym $mod+Shift+Down move down
bindsym $mod+Shift+Up move up
bindsym $mod+Shift+Right move right

workspace 1 output primary
workspace 2 output primary
workspace 3 output eDP-1 eDP1 primary
workspace 4 output eDP-1 eDP1 primary
workspace 5 output eDP-1 eDP1 primary

# move workspaces between monitors
bindsym $mod+Shift+Ctrl+h move workspace to output left
bindsym $mod+Shift+Ctrl+j move workspace to output down
bindsym $mod+Shift+Ctrl+k move workspace to output up
bindsym $mod+Shift+Ctrl+l move workspace to output right

assign [class="URxvt"] 1
assign [class="Code"] 2
assign [class="Firefox"] 3
assign [class="URxvt" instance="irssi"] 4
assign [class="Pidgin"] 4
assign [class="VirtualBox Machine"] 5
assign [class="VirtualBox Manager"] 5

# Some windows are better floating, as they will be used for "focused" or short tasks
# or to drag and drop between than, or just because i want
for_window [class="Nm-connection-editor"] floating enable
for_window [class="VirtualBox Manager"] floating enable
for_window [class="Firefox" instance="Places"] floating enable

# split in horizontal orientation
bindsym $mod+semicolon split h

# split in vertical orientation
bindsym $mod+minus split v

# enter fullscreen mode for the focused container
bindsym $mod+f fullscreen toggle

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+s layout stacking
bindsym $mod+w layout tabbed
bindsym $mod+e layout toggle split

# toggle tiling / floating
bindsym $mod+Shift+space floating toggle

# change focus between tiling / floating windows
bindsym $mod+space focus mode_toggle

# focus the parent container
bindsym $mod+a focus parent

# focus the child container
#bindsym $mod+d focus child

# switch to workspace
bindsym $mod+1 workspace 1
bindsym $mod+2 workspace 2
bindsym $mod+3 workspace 3
bindsym $mod+4 workspace 4
bindsym $mod+5 workspace 5
bindsym $mod+6 workspace 6
bindsym $mod+7 workspace 7
bindsym $mod+8 workspace 8
bindsym $mod+9 workspace 9
bindsym $mod+0 workspace 10

# move focused container to workspace
bindsym $mod+Shift+1 move container to workspace 1
bindsym $mod+Shift+2 move container to workspace 2
bindsym $mod+Shift+3 move container to workspace 3
bindsym $mod+Shift+4 move container to workspace 4
bindsym $mod+Shift+5 move container to workspace 5
bindsym $mod+Shift+6 move container to workspace 6
bindsym $mod+Shift+7 move container to workspace 7
bindsym $mod+Shift+8 move container to workspace 8
bindsym $mod+Shift+9 move container to workspace 9
bindsym $mod+Shift+0 move container to workspace 10

# reload the configuration file
bindsym $mod+Shift+c reload
# restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
bindsym $mod+Shift+r restart
# exit i3 (logs you out of your X session)
bindsym $mod+Shift+e exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -b 'Yes, exit i3' 'i3-msg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
        # These bindings trigger as soon as you enter the resize mode

        # Pressing left will shrink the window’s width.
        # Pressing right will grow the window’s width.
        # Pressing up will shrink the window’s height.
        # Pressing down will grow the window’s height.
        bindsym j resize shrink width 10 px or 10 ppt
        bindsym k resize grow height 10 px or 10 ppt
        bindsym l resize shrink height 10 px or 10 ppt
        bindsym semicolon resize grow width 10 px or 10 ppt

        # same bindings, but for the arrow keys
        bindsym Left resize shrink width 10 px or 10 ppt
        bindsym Down resize grow height 10 px or 10 ppt
        bindsym Up resize shrink height 10 px or 10 ppt
        bindsym Right resize grow width 10 px or 10 ppt

        # back to normal: Enter or Escape
        bindsym Return mode "default"
        bindsym Escape mode "default"
}

bindsym $mod+r mode "resize"

# Start i3bar to display a workspace bar (plus the system information i3status
# finds out, if available)
bar {
	position top

        output primary
        tray_output primary

        status_command i3status

	colors {
                background #002b36
                statusline #ffffff
                separator #ffffff
                focused_workspace #2aa198 #2aa198 #ffffff
                active_workspace #93a1a1 #93a1a1 #002b36
                inactive_workspace #586e75 #586e75 #93a1a1
                urgent_workspace #dc322f #dc322f #ffffff
	}
}

bar {
	position top

        output eDP-1
        tray_output primary
        
	colors {
                background #002b36
                statusline #ffffff
                separator #ffffff
                focused_workspace #2aa198 #2aa198 #ffffff
                active_workspace #93a1a1 #93a1a1 #002b36
                inactive_workspace #586e75 #586e75 #93a1a1
                urgent_workspace #dc322f #dc322f #ffffff
	}
}

focus_follows_mouse no

# class                 border  backgr. text    indicator child_border
client.focused          #2aa198 #2aa198 #ffffff #2aa198   #2aa198
client.focused_inactive #93a1a1 #93a1a1 #ffffff #93a1a1   #93a1a1
client.unfocused        #586e75 #586e75 #93a1a1 #586e75   #586c75
client.urgent           #dc322f #dc322f #ffffff #dc322f   #dc322f
client.placeholder      #002b36 #002b36 #ffffff #002b36   #002b36

client.background       #ffffff

default_border pixel 3
default_floating_border pixel 3

# custom configurations 
bindsym Ctrl+mod1+l exec i3lock -c 173f4f

exec_always --no-startup-id setxkbmap -layout br,us -variant abnt2,intl -option "shift:both_capslock_cancel,ctrl:nocaps,grp:rctrl_rshift_toggle"
exec --no-startup-id /home/giuliani/.local/sbin/setupmonitor guess
exec --no-startup-id xfsettingsd
exec --no-startup-id dunst
exec --no-startup-id feh --bg-scale ~/.wallpaper
exec --no-startup-id nm-applet
exec --no-startup-id blueman-applet
exec --no-startup-id thunar --daemon
exec --no-startup-id xfce4-volumed-pulse
# xinput list | grep Touchpad
# xinput list-props <id_number_from_above_command>
# enable tapping on my touchpad. To discover your device id and property use:
# exec --no-startup-id xinput set-prop 16 310 1
# Disable tapping while typing
# exec --no-startup-id xinput set-prop 16 318 1
# exec --no-startup-id xinput set-prop 16 319 1
